

# README #

This is sample djanog project. 

* Clone it locally
* install Django 
* run migration. 
* Follow the steps below to complete your task(s)
### Steps of Task - 1 ###

* Create a web page to to capture three integers
* Store those all in three differen cloumns of your newly created table. 
* Inside 4th column of this table, store number of groups of integers (1, 2 or 3). Like if all three integers are different those belong to 3 groups, if 2 values are same and one is different then they belong to 2 groups. If all are same they belong to 1 group.
* Show your saved data on a web page with all three values and number of groups of integer stored in 4th column.

### Steps of Task - 2 ###

* For task one, create API's (REST) (One api to take three inputs, One to show the stored data)
* Perform those API calls from any API client like Postman


### What is this repository for? ###

* Online Tasks Evaluation
* [Find code here](https://bitbucket.org/mohsinalexa/sampledjangoapp/src/master/)

